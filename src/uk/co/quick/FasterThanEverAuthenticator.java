package uk.co.quick;

public class FasterThanEverAuthenticator implements Authenticator {

    @Override
    public LoginResult login(final Credentials credentials) {
        return LoginResult.AUTHENTICATED;
    }

}
