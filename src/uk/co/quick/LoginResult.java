package uk.co.quick;

public enum LoginResult {
    AUTHENTICATED, INVALID_CREDENTIALS, ERROR
}
