package uk.co.quick;

public interface Authenticator {

    LoginResult login(Credentials credentials);

}
